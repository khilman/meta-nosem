KBRANCH_amlogic ?= "standard/base"

KCONFIG_MODE_amlogic ?= "--alldefconfig"
KBUILD_DEFCONFIG_amlogic ?= "defconfig"

COMPATIBLE_MACHINE_amlogic = "amlogic"
