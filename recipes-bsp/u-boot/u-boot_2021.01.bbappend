FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}/${PV}:"

# Add p241 support
SRC_URI_append = " file://0001-arm64-meson-import-p241-dts-from-linux-v5.8-rc1.patch \
	       	   file://0002-arm64-meson-add-support-for-the-amlogic-p241-board.patch"

# Most platform just specify the dtb basename but meson adds the "amlogic/" dir.
# We could force yocto to install the dtb in an "amlogic/" dir but its easier to just
# amend the fdtfile variable to match yocto default behavior
SRC_URI_append = " file://0001-meson-env-remove-amlogic-directory-from-fdtfile-vari.patch"

# HDMI and USB Keyboard mess with the TTY and are annoying while working on the platform
SRC_URI_append = "${@bb.utils.contains('UBOOT_ILOVEUART', '1', " file://meson-config-only-uart-by-default.patch", "", d)}"

